package ru.t1.godyna.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.model.IWBS;
import ru.t1.godyna.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(name = "name", nullable = false, length = 50)
    private String name = "";

    @NotNull
    @Column(name = "descrptn", nullable = false, length = 200)
    private String description = "";

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public Task(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description + " : " + this.getId();
    }

}
