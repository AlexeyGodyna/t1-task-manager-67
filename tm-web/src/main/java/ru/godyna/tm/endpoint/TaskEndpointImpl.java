package ru.godyna.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.godyna.tm.api.endpoint.ITaskRestEndpoint;
import ru.godyna.tm.model.Task;
import ru.godyna.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.godyna.tm.api.endpoint.ITaskRestEndpoint")
public class TaskEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @Nullable
    @GetMapping("/findAll")
    public  Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.findOneById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskService.add(task);
        return task;
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskService.remove(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        taskService.removeById(id);
    }

}
