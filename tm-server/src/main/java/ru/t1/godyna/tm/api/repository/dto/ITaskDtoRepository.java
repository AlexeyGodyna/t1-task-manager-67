package ru.t1.godyna.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.godyna.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    long countByUserId(String userId);

    boolean existsByUserIdAndId(String userId, String id);

    @Nullable
    TaskDTO findByUserIdAndId(String userId, String id);

    @Nullable
    List<TaskDTO> findAllByUserId(String userId);

    @Transactional
    void deleteByProjectId(String projectId);

    @Transactional
    void deleteByUserId(String userId);

    @Transactional
    void deleteByUserIdAndId(String userId, String id);

    @Transactional
    void deleteAll();

    @Nullable
    @Query("SELECT p FROM TaskDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<TaskDTO> findAllByUserIdWithSort(@NotNull String userId, @NotNull String sortType);

    @Nullable
    List<TaskDTO> findAllByUserIdAndProjectId(String userId, String projectId);

}
